all: main main-pthread main-openmp

main: src/main.cpp src/StdLib.cpp
	g++ -std=c++0x $^ -o $@

main-pthread: src-pthread/main.cpp src-pthread/StdLib.cpp
	g++ -std=c++0x $^ -o $@ -lpthread
	
main-openmp: src-openmp/main.cpp src-openmp/StdLib.cpp
	g++ -std=c++0x $^ -o $@ -lpthread -fopenmp
	
clean:
	rm main main-pthread main-openmp
