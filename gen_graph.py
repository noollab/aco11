import subprocess
import sys
import matplotlib.pyplot as pyplot


def go(program, input_filename, n_tests, sequencial_time, max_threads):
    speedup = []
    
    for i in range(1,max_threads+1):
        time_sum = 0.0
        for j in range(n_tests):
            output = subprocess.check_output(['./'+program,
                                              str(i),
                                              input_filename])
            time_sum += float(output)
        time_mean = time_sum/n_tests
        speedup.append(sequencial_time/time_mean)
        print i, time_mean, sequencial_time/time_mean
    return speedup
    

def main():
    if len(sys.argv) != 4:
        print('usage: gen_graph <max_threads> <input> <output>')
        sys.exit(1)

    max_threads = int(sys.argv[1])
    input_filename = sys.argv[2]
    output_filename = sys.argv[3]
    tests_per_n_thread = 1
    
    sequencial_time = 0.0
    time_sum = 0.0
    for i in range(tests_per_n_thread):
        output = subprocess.check_output(['./main', input_filename])
        time_sum += float(output)
    sequencial_time = time_sum/tests_per_n_thread
    print 's', sequencial_time, float(1.0)
    
    x = list(range(1,max_threads+1))

    su_pthread = go('main-pthread',
                    input_filename,
                    tests_per_n_thread,
                    sequencial_time,
                    max_threads)

    su_openmp = go('main-openmp',
                   input_filename,
                   tests_per_n_thread,
                   sequencial_time,
                   max_threads)

    # 
    pyplot.figure(figsize=(8,6))
    pyplot.xlabel('threads')
    pyplot.ylabel('tempo/speed_up')
    #
    pyplot.plot(x, x, label='ideal')
    pyplot.plot(x, su_pthread, label='pthread')
    pyplot.plot(x, su_openmp, label='openmp')
    #
    pyplot.legend(loc='upper left')
    pyplot.savefig(output_filename)
    

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('You murder')