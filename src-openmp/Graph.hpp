#ifndef __GRAPH__
#define __GRAPH__

struct Edge{
	double heuristic;
	double pheromone;
	double cost;
	Edge(double _heuristic=0, double _pheromone=0, double _cost=0):
		heuristic(_heuristic),
		pheromone(_pheromone),
		cost(_cost)
	{}
};

typedef vector<Edge> VE;
typedef vector<VE> VVE;

class Graph{
public:
	Graph(VVE &_adj_mtx): 
		adj_mtx(_adj_mtx),
		n_nodes(_adj_mtx.size())
	{}
	
	Graph(){}
	
	/*void set_pheromone(int u, int v, int p){
		assert(u>=0 and u<n_nodes);
		assert(v>=0 and v<n_nodes);
		
		bool flag = false;
		
		for(PIE &pie : adj_list[u]){
			if(pie.first == v){
				pie.second.pheromone = p;
				flag = true;
				break;
			}
		}
		assert(flag);
		
		for(PIE &pie : adj_list[v]){
			if(pie.first == u){
				pie.second.pheromone = p;
				flag = true;
				break;
			}
		}
		assert(flag);		
	}
	
	double get_pheromone(int u, int v){
		assert(u>=0 and u<n_nodes);
		assert(v>=0 and v<n_nodes);
		
		if(adj_list[v].size() < adj_list[u].size())
			swap(u,v);
		
		for(PIE &pie : adj_list[u]){
			if(pie.first == v)
				return pie.second.pheromone;
		}
		assert(1 == 2);
		return -INF;
	}
	
	double get_heuristic(int u, int v){
		assert(u>=0 and u<n_nodes);
		assert(v>=0 and v<n_nodes);
		
		if(adj_list[v].size() < adj_list[u].size())
			swap(u,v);
		
		for(PIE &pie : adj_list[u]){
			if(pie.first == v)
				return pie.second.heuristic;
		}
		assert(1 == 2);
		return -INF;
	}
	
	double get_cost(int u, int v){
		assert(u>=0 and u<n_nodes);
		assert(v>=0 and v<n_nodes);
		
		if(adj_list[v].size() < adj_list[u].size())
			swap(u,v);
		
		for(PIE &pie : adj_list[u]){
			if(pie.first == v)
				return pie.second.cost;
		}
		assert(1 == 2);
		return INF;
	}*/
	
	VVE adj_mtx;
	int n_nodes;
};

#endif
