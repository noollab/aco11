#ifndef __STD_LIB__
#define __STD_LIB__

#include <vector>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <utility>
#include <cassert>
#include <cmath>
#include <thread>
#include <fstream>
#include <random>
#include <unistd.h>
#include <omp.h>
#include <sys/time.h>

#define all(a) a.begin(), a.end()
using namespace std;


int rand_btw(int a, int b);
double getTime();

const int INF = 1<<30;

typedef vector<int> VI;
typedef vector<double> VD;

typedef vector<int> VVI;



#endif
