#ifndef __ANT__
#define __ANT__

#include "Graph.hpp"

class Ant{
public:
	Ant(int n_nodes):path_cost(0), is_visited(n_nodes, false){}

	void add_node(int v){
		path.push_back(v);
		is_visited[v] = true;
	}
	
	void go(Graph &graph, double alpha, double beta){
		this->add_node(0);
		VI candidates;
		VD prob;
		double total_prob, p1, p2;
		path_cost = 0;
		for(int i=0; i<(int)graph.n_nodes-1; i++){
			int u = path[path.size()-1];
			candidates.clear();
		
			total_prob = 0;
			for(int v=0; v<graph.n_nodes; v++) if(not is_visited[v]){
				p1 = pow(graph.adj_mtx[u][v].heuristic, alpha);
				p2 = pow(graph.adj_mtx[u][v].pheromone, beta);
				total_prob += p1*p2;
				candidates.push_back(v);
			}
			
			prob.assign(candidates.size(), 0.0);
			for(int i=0; i<(int)prob.size(); i++){
				if(i)
					prob[i] = prob[i-1];
				int v = candidates[i];
				
				double v1 = pow(graph.adj_mtx[u][v].heuristic, alpha);
				double v2 = pow(graph.adj_mtx[u][v].pheromone, beta);
				prob[i] += (v1*v2)/total_prob;
			}
			
			double nrand = double(rand_btw(0, 100))/100.0;
			
			int w = lower_bound(all(prob),nrand) - prob.begin();
			if(w == (int)prob.size())
				w--;
			
			w = candidates[w];	
			
			this->add_node(w);
			path_cost += graph.adj_mtx[u][w].cost;
		}
		assert(path.size() > 0);
		
		int u = path[0];
		int v = path[path.size()-1];
		path_cost += graph.adj_mtx[u][v].cost;
	}
	
	bool operator<(Ant &other){
		if(is_visited.size() == 0)
			return false;
		else if(other.is_visited.size() == 0)
			return true;
		return path_cost < other.path_cost;
	}

	VI path;
	double path_cost;
	VI is_visited;
};

#endif
