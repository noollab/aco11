#ifndef __ENVIRONMENT__
#define __ENVIRONMENT__

#include "StdLib.hpp"
#include "Graph.hpp"
#include "Ant.hpp"

#include <mutex>

std::mutex mutex_it;

class Environment;

void sub_aco(Environment *env, int _n_ants, double alpha, double beta);

class Environment{
public:
	Environment(Graph _graph, int _n_ants):
		graph(_graph),
		n_ants(_n_ants),
		it_best_ant(0)
	{}
	
	Ant aco(int n_threads, int its, double Q, double rho, 
		double alpha=1.0, double beta=1.0)
	{ 
		init_environment();
		Ant flik(0);
		flik.path_cost = INF;
		
		int na_all = n_ants/n_threads;
		int na_last = na_all + n_ants%n_threads;
		
		vector<thread> threads(n_threads);
		while(its--){
			
			it_best_ant = Ant(0);
			it_best_ant.path_cost = INF;
		
			for(int i=0; i<n_threads-1; i++){
				threads[i] = thread(sub_aco, this, na_all, alpha, beta);
			}
			threads[n_threads-1] = thread(sub_aco, this, na_last, alpha, beta);
			
			for(thread &thr : threads)
				thr.join();
			
			//evapora o feromonio
			evaporate_environment(rho);
			
			//intensifica o feromonio no caminho mais curto percorrid
			reinforce_path(Q, it_best_ant.path);
			
			//guarda a melhor das melhores soluções
			if(it_best_ant < flik)
				flik = it_best_ant;
		}
		return flik;
	}
	Graph graph;
	int n_ants;
	Ant it_best_ant;
	
private:
	void init_environment(){
		for(int u=0; u<graph.n_nodes; u++){
			for(int v=0; v<graph.n_nodes; v++){
				graph.adj_mtx[u][v].pheromone = 1.0;
				
				int cost = graph.adj_mtx[u][v].cost;
				graph.adj_mtx[u][v].heuristic = 1.0/cost;
			}
		}
	}
	
	void reinforce_path(double Q, VI &path){
		int u,v;
		double p;
		for(int i=0; i<graph.n_nodes-1; i++){
			u = path[i];
			v = path[i+1];
			
			p = graph.adj_mtx[u][v].pheromone;
			graph.adj_mtx[u][v].pheromone += p*Q ;
		}
		u = path[graph.n_nodes-1];
		v = path[0];
		p = graph.adj_mtx[u][v].pheromone;
		graph.adj_mtx[u][v].pheromone += p*Q ;
	}
	
	void evaporate_environment(double rho){
		rho = 1.0 - rho;
		for(int u=0; u<graph.n_nodes; u++){
			for(int v=0; v<graph.n_nodes; v++){
				graph.adj_mtx[u][v].pheromone *= rho;
			}
		}
	}
};

void sub_aco(Environment *env, int _n_ants, double alpha, double beta){
	vector<Ant> ants(_n_ants, Ant(env->graph.n_nodes));

	Ant best_ant = ants[0];
	best_ant.path_cost = INF;
	for(Ant &ant : ants){
		ant.go(env->graph, alpha, beta);
		if( ant.path_cost < best_ant.path_cost)
			best_ant = ant;
	}
	
	mutex_it.lock();
	if(best_ant < env->it_best_ant){
		env->it_best_ant = best_ant;
	}
	mutex_it.unlock();
}


#endif
