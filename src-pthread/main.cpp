#include "StdLib.hpp"
#include "Environment.hpp"
#include "Ant.hpp"
#include "Graph.hpp"

extern struct drand48_data drand_buffer;

double dist(double x1, double y1, double x2, double y2){
	return hypot(abs(x1-x2), abs(y1-y2));
}

int main(int argc, char **argv){
	if(argc != 3) {
		printf("Usage:\n\tmain <n_threads> <input_filename>\n\n");
		return 1;
	}
	
	struct timeval tv;
	gettimeofday(&tv, NULL);
	srand48_r(tv.tv_sec * 238479 + tv.tv_usec, &drand_buffer);
	
	fstream file(argv[2]);
	
    srand(time(NULL));
    double x[1000],y[1000];
    double begin_time, end_time;
    int chato, n, t, c;
    file >> t >> n;
  
    VVE adj_mtx(n, VE(n));
    if(t == 1){
		for(int i=0; i<n; i++){
			file >> chato >> x[i] >> y[i];
		}
		 
		for(int u=0; u<n; u++){
			for(int v=0; v<n; v++){
				adj_mtx[u][v] = Edge(0, 0, dist(x[u],y[u],x[v],y[v]));
			}
		}
	}else{
		for(int u=0; u<n; u++){
			for(int v=0; v<n; v++){
				file >> c;
				adj_mtx[u][v] = Edge(0, 0, c);
			}
		}
	}

	Graph graph(adj_mtx);
    Environment env( graph, 100);
    begin_time = getTime();
    Ant flik = env.aco(atoi(argv[1]), 1000, 0.05, 0.05);
    end_time = getTime();
    //printf("Flik path cost: %lf\n",flik.path_cost);
    //printf("Time for ACO: %lf\n",end_time-begin_time);
    cout << end_time-begin_time << "\n";
 
    return 0;
}
