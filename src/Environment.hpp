#ifndef __ENVIRONMENT__
#define __ENVIRONMENT__

#include "StdLib.hpp"
#include "Graph.hpp"
#include "Ant.hpp"

class Environment{
public:
	Environment(Graph _graph, int _n_ants):
		graph(_graph),
		n_ants(_n_ants)
	{}
	
	Ant aco(int its, double Q, double rho, 
		double alpha=1.0, double beta=1.0)
	{ 
		init_environment();
		Ant flik(0);
		flik.path_cost = INF;
		
		while(its--){
			vector<Ant> ants;
			for(int i=0; i<n_ants; i++)
				ants.push_back(Ant(graph.n_nodes));
			
			//Cada formiga anda por um caminho
			for(Ant &ant : ants){
				ant.go(graph, alpha, beta);
			}
			
			//evapora o feromonio
			evaporate_environment(rho);
			
			//escolher a formiga que percorreu o caminho mais curto
			Ant best_ant = ants[0];
			for(Ant &ant : ants){
				if( ant.path_cost < best_ant.path_cost)
					best_ant = ant;
			}
			
			//intensifica o feromonio no caminho mais curto percorrido
			reinforce_path(Q, best_ant.path);
			
			//guarda a melhor das melhores soluções
			if(best_ant < flik)
				flik = best_ant;
		}
		return flik;
	}
	
private:
	void init_environment(){
		for(int u=0; u<graph.n_nodes; u++){
			for(int v=0; v<graph.n_nodes; v++){
				graph.adj_mtx[u][v].pheromone = 1.0;
				
				int cost = graph.adj_mtx[u][v].cost;
				graph.adj_mtx[u][v].heuristic = 1.0/cost;
			}
		}
	}
	
	void reinforce_path(double Q, VI path){
		for(int i=0; i<graph.n_nodes; i++){
			int u = path[i];
			int v = path[(i+1)%graph.n_nodes];
			
			double p = graph.adj_mtx[u][v].pheromone;
			graph.adj_mtx[u][v].pheromone += p*Q ;
		}
	}
	
	void evaporate_environment(double rho){
		for(int u=0; u<graph.n_nodes; u++){
			for(int v=0; v<graph.n_nodes; v++){
				graph.adj_mtx[u][v].pheromone *= (1.0-rho);
			}
		}
	}
	
	Graph graph;
	int n_ants;
};

#endif
