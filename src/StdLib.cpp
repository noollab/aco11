#include "StdLib.hpp"

struct drand48_data drand_buffer;

int rand_btw(int a, int b){
	double x;
	drand48_r(&drand_buffer, &x);
	return x*(b - a);
}

double getTime() { /* para Unix/Linux */
    struct timeval  tv;
    struct timezone tz;
    gettimeofday(&tv, &tz);
    double milliseconds = 1000.0 * ((double)tv.tv_sec + (double)tv.tv_usec/1000000.0);
    return milliseconds / 1000.0;
}
